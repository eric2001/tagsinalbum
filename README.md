# TagsInAlbum
![TagsInAlbum](./screenshot.jpg) 

## Description
TagsInAlbum is a module for Gallery 3 which adds a sidebar block to album pages that contains a list of tags used by photos/videos/albums within that album. This list does not include the contents of sub-albums.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "tagsinalbum" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu. Once activated you will be able to control the placement of this item from the Appearance -> Manage sidebar menu.

## History
**Version 1.2.0:**
> - Re-worked database query to handle removing duplicate tag names, sorting tags by name, and limiting the number of tags displayed.
> - Updated module.info file for recent changes in Gallery 3.
> - Tested with Gallery 3.0.2.
> - Released 11 June 2011.
>
> Download: [Version 1.2.0](/uploads/2a0f4b00d92ba931b3a3de860f583a14/tagsinalbum120.zip)

**Version 1.1.0:**
> - Added a configuration option to limit the number of tags that are displayed.
> - Released 21 April 2011.
>
> Download: [Version 1.1.0](/uploads/4b0a038b6075fa0e1e8fb1fbe2dc327d/tagsinalbum110.zip)

**Version 1.0.1:**
> - Added warning messages to alert the user that this module requires the Tags module in order to function properly.
> - Released on 31 January 2011.
>
> Download: [Version 1.0.1](/uploads/26bb231cf091a592e13f81119ae37afc/tagsinalbum101.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 14 November 2010.
>
> Download: [Version 1.0.0](/uploads/f3bf406a126d3e221627c2334bc5f16b/tagsinalbum100.zip)
